# from django.contrib.auth.models import User
from .models import Expositor
from debian_day_form.users.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # fields = '__all__'
        fields = ('id', 'username', 'name', 'first_name', 'last_name')


class ExpositorSerializer(serializers.ModelSerializer):
    # user = serializers.PrimaryKeyRelatedField(many=True, queryset=Expositor.objects.all())

    user = UserSerializer(required=True)

    class Meta:
        model = Expositor
        fields = '__all__'
        # fields = ('user', 'community', 'title', 'detail')

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        expositor, created = Expositor.objects.update_or_create(
            user=user,
            subject_major=validated_data.pop('subject_major')
        )
        return expositor