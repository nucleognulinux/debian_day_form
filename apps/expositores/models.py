from django.db import models

from debian_day_form.users.models import User


class Expositor(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    community = models.CharField(
        max_length=100, 
    )
    title = models.CharField(
        max_length=100,
    )
    
    detail = models.CharField(
        max_length=500
    )
    

    class Meta:
        verbose_name_plural = "expositores"

    def __str__(self):
        return str(self.tema)