from django.apps import AppConfig


class ExpositoresConfig(AppConfig):
    name = 'expositores'
