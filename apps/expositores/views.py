from django.shortcuts import render
from .serializers import ExpositorSerializer
from rest_framework import generics

# Create your views here.
# from django.contrib.auth.models import User
from .models import Expositor

class ExpositorList(generics.ListAPIView):
    queryset = Expositor.objects.all()
    serializer_class = ExpositorSerializer


# class UserDetail(generics.RetrieveAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
