from django.contrib import admin

from .models import Expositor


admin.site.register(Expositor)
